package com.keithwilliams;

import java.util.Map;
import java.util.TreeMap;

public class ConvertNumeralsToNumbers {
	int number=0;
	int lastNumeralValue=0;
	
	public int convertToNumber(String numeral) {
		TreeMap <String, Integer> map = new TreeMap <String, Integer>();
		map.put("I", 1);
		map.put("V", 5);
		map.put("X", 10);
		map.put("L", 50);
		map.put("C", 100);
		map.put("D", 500);
		map.put("M", 1000);
		
		if (numeral.length()==1) {
			 number= map.get(numeral);
		} else if (numeral.length() % 2==0) {
			for (int i=0; i < numeral.length()-1; i+=2) {
				String letter =numeral.substring(i, i+1);
				String nextLetter=numeral.substring(i+1, i+2);
					if (map.get(letter) >= map.get(nextLetter)) {
						number += map.get(letter) + map.get(nextLetter);
					} else {
						number += map.get(nextLetter) - map.get(letter);
					}
			}
		} else {
			for (int i=0; i < numeral.length()-2; i+=2) {
				String letter =numeral.substring(i, i+1);
				String nextLetter=numeral.substring(i+1, i+2);
				String lastLetter=numeral.substring(numeral.length()-1);
				lastNumeralValue =map.get(lastLetter);
					if (map.get(letter) >= map.get(nextLetter)) {
						number += map.get(letter) + map.get(nextLetter);
					} else {
						number += map.get(nextLetter) - map.get(letter);
					}
			}
		}
		return number+lastNumeralValue;
	}
}
