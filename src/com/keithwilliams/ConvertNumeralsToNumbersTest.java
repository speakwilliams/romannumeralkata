package com.keithwilliams;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ConvertNumeralsToNumbersTest {
	
	ConvertNumeralsToNumbers numerals; 

	
	@Before
	public void setUp() {
		numerals= new ConvertNumeralsToNumbers();
	}
	
	@Test
	public void whenGivenStringIOneIsReturned() {
		
		assertEquals(1, numerals.convertToNumber("I"));
	}
	@Test
	public void whenGivenStringVFiveIsReturned() {
		
		assertEquals(5, numerals.convertToNumber("V"));
	}
	@Test
	public void whenGivenStringXTenIsReturned() {
		
		assertEquals(10, numerals.convertToNumber("X"));
	}
	@Test
	public void whenGivenAKeyTheValueisReturned() {
		
		assertEquals(100, numerals.convertToNumber("C"));

	}
	@Test
	public void whenGivenStringIITwoIsReturned() {
		
		assertEquals(2, numerals.convertToNumber("II"));
	}
	@Test
	public void whenGivenStringIXNineIsReturned() {
		
		assertEquals(9, numerals.convertToNumber("IX"));
	}
	@Test
	public void whenGivenStringXLFortyIsReturned() {
		
		assertEquals(40, numerals.convertToNumber("XL"));
	}
	@Test
	public void whenGivenStringCIOneHundredOneIsReturned() {
		
		assertEquals(101, numerals.convertToNumber("CI"));
	}
	@Test
	public void whenGivenStringXXITwentyOneIsReturned() {
		
		assertEquals(21, numerals.convertToNumber("XXI"));
	}
	@Test
	public void whenGivenStringXXXXFortyIsReturned() {
		
		assertEquals(40, numerals.convertToNumber("XXXX"));
	}
	@Test
	public void whenGivenStringDCCCXXXVIIEightHundredThirtySevenIsReturned() {
		
		assertEquals(837, numerals.convertToNumber("DCCCXXXVII"));
	}
	@Test
	public void whenGivenStringMMMMCMXCIXFourThousandNinetyNineIsReturned() {
		
		assertEquals(4999, numerals.convertToNumber("MMMMCMXCIX"));
	}
	@Test
	public void whenGivenStringMLXVIIIOneThousandSixtyEightIsReturned() {
		
		assertEquals(1066, numerals.convertToNumber("MLXVI"));
	}

}
