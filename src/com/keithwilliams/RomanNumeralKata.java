package com.keithwilliams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RomanNumeralKata {

	public String convertToNumeral(int number) {
		String numeral="";
		String roman="";
		
		List <String> empty = new ArrayList<String>();
		List <String> ones = new ArrayList<String>();
		List <String> tens = new ArrayList<String>();
		List <String> hunds = new ArrayList<String>();
		List <String> thous = new ArrayList<String>();
		
		empty.addAll(Arrays.asList(""));
		ones.addAll(Arrays.asList("", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"));
		tens.addAll(Arrays.asList("", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" ));
		hunds.addAll(Arrays.asList("", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"));
		thous.addAll(Arrays.asList("", "M", "MM", "MMM", "MMMM" ));
		
		
		List <List<String>> romanNumeralList = new ArrayList<List<String>>();
		romanNumeralList.add(0, empty);
		romanNumeralList.add(1, ones);
		romanNumeralList.add(2, tens);
		romanNumeralList.add(3, hunds);
		romanNumeralList.add(4, thous);

		int numberLength=Integer.toString(number).length();
		String stringNumber= Integer.toString(number);
		
		for (int i=0; i < numberLength; i++) {
			numeral =romanNumeralList.get(numberLength-i).get(Integer.parseInt(String.valueOf(stringNumber.charAt(i))));
			StringBuilder builder = new StringBuilder();
			 roman +=builder.append(numeral).toString();
		}		 
		
		return roman;
	
	}
}
