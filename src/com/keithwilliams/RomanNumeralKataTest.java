package com.keithwilliams;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RomanNumeralKataTest {
	
	RomanNumeralKata number;
	
	@Before
	public void setUp() {
		number= new RomanNumeralKata();
	}
	@Test
	public void whenGivenintOneReturnsStringI() {

		assertEquals("I", number.convertToNumeral(1));
	}
	@Test
	public void whenGivenintTwoReturnsStringII() {
		
		assertEquals("II", number.convertToNumeral(2));
		
	}
	@Test
	public void whenGivenintThreeReturnsStringIII() {
		
		assertEquals("III", number.convertToNumeral(3));
		
	}
	@Test
	public void whenGivenintFourReturnsStringIV() {
		
		assertEquals("IV", number.convertToNumeral(4));
		
	}
	@Test
	public void whenGivenintFiveReturnsStringV() {
		
		assertEquals("V", number.convertToNumeral(5));
		
	}
	@Test
	public void whenGivenintSixReturnsStringVI() {
		
		assertEquals("VI", number.convertToNumeral(6));
		
	}
	@Test
	public void whenGivenintTenReturnsStringX() {
		
		assertEquals("X", number.convertToNumeral(10));
		
	}
	@Test
	public void whenGivenintThirteenReturnsStringXIII() {
		
		assertEquals("XIII", number.convertToNumeral(13));
		
	}
	@Test
	public void whenGivenintThirtyThreeReturnsStringXXXIII() {
		
		assertEquals("XXXIII", number.convertToNumeral(33));
		
	}
	@Test
	public void whenGivenintWithtwoDigitsCorrectstringReturns() {
		
		assertEquals("LVII", number.convertToNumeral(57));
		assertEquals("XCIX", number.convertToNumeral(99));
		
	}
	@Test
	public void whenGivenIntOneHunderdOneReturnsStringCI() {
		
		assertEquals("CI", number.convertToNumeral(101));
		
	}
	@Test
	public void whenGivenIntNineHunderdNinetyNineStringCMXCIX() {
		
		assertEquals("CMXCIX", number.convertToNumeral(999));
		
	}
	@Test
	public void whenGivenintWithThreeDigitsCorrectStringReturns() {
		
		assertEquals("DCCLXIII", number.convertToNumeral(763));
		assertEquals("CCCLXXXVII", number.convertToNumeral(387));
		
	}
	@Test
	public void whenGivenInOneThousandOneStringMI() {
		
		assertEquals("MI", number.convertToNumeral(1001));
		
	}
	@Test
	public void whenGivenIntTwoThousandFourHundredSeventyStringMMCDLXX() {
		
		assertEquals("MMCDLXX", number.convertToNumeral(2470));
		
	}
	@Test
	public void whenGivenintWithFourDigitsCorrectStringReturns() {
		
		assertEquals("MMMMCMXCIX", number.convertToNumeral(4999));
		assertEquals("MMMCCVIII", number.convertToNumeral(3208));
		
	}
	

}
